import { format } from "path"
import { Material } from "../types/material"

export interface StockData {
    material: string
    productCode: string
    locQty: LocQty[]
}

interface LocQty {
    location: string
    qty: number
}
export class StockRepository {
    private products = [
        {
            ID: 1,
            material: "MAT0001",
            productCode: "PC000001",
            location: "A1",
            qty: 100,
        }, {
            ID: 2,
            material: "MAT0001",
            productCode: "PC000001",
            location: "A2",
            qty: 100,
        }, {
            ID: 3,
            material: "MAT0001",
            productCode: "PC000001",
            location: "A4",
            qty: 100,
        }, {
            ID: 4,
            material: "MAT0002",
            productCode: "PC000002",
            location: "A1",
            qty: 100,
        }, {
            ID: 5,
            material: "MAT0002",
            productCode: "PC000002",
            location: "A3",
            qty: 100,
        }, {
            ID: 6,
            material: "MAT0003",
            productCode: "PC000003",
            location: "A1",
            qty: 100.
        }, {
            ID: 7,
            material: "MAT0004",
            productCode: "PC000004",
            location: "A2",
            qty: 100,
        },
        {
            ID: 8,
            material: "MAT0004",
            productCode: "PC000004",
            location: "A5",
            qty: 100,
        }

    ]
    async getMaterialStocks(): Promise<Material[]> {
        return this.products
    }

    getAggregateStockData(): StockData[] {

        const stockData: StockData[] = [];

        this.products.forEach(product => {
            let stockItem = stockData.find(s => s.material === product.material && s.productCode === product.productCode);

            if (stockItem) {
                
                stockItem.locQty.push({ location: product.location, qty: product.qty });
            } else {
                
                stockData.push({
                    material: product.material,
                    productCode: product.productCode,
                    locQty: [{ location: product.location, qty: product.qty }],
                });
            }
        });

        let tableHead: string[] = []
        this.products.forEach((product) => {
            if (tableHead.indexOf(product.location) === -1) {
                tableHead.push(product.location)
            }
        })
        tableHead.sort((a, b) => a.localeCompare(b))
        tableHead.unshift("Material")
        tableHead.push("Sum")

        const tableData = stockData.map(item => {
            
            const row: any = { Material: item.material, Sum: 0, fields: [] };
            tableHead.forEach(location => {

                if (location !== "Material" && location !== "Sum") {
                    row.fields.push({ loc: location, value: 0 }); 
                }
                
            });

            
            item.locQty.forEach(locQty => {
                let field = row.fields.find(field => field.loc === locQty.location);
                if (field) {
                    field.value = locQty.qty;
                    row.Sum += locQty.qty; 
                }
            });

            return row;
        });


        return tableData;
    }





}



