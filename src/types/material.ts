export interface Material {
    ID: number
    material: string
    productCode: string
    location: string
    qty: number
}