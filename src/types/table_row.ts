interface LocationValue {
    location: string
    value: string
}
export interface TextFieldTableRow {

    material: string
    textFields: LocationValue[]
    sum: number

}